package com.devcamp.task56b70.models;

public class Dog extends Mammal {
    public Dog(String name){
        super(name);
    }

    @Override
    public String toString() {
        return "Dog [" + super.toString() + "]";
    }

    public void greets(){
        System.out.println("Wooof");
    }

    public void greets(Dog another){
        System.out.println("Woooooooof");
    }
    
}
