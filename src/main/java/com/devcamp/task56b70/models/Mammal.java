package com.devcamp.task56b70.models;

public class Mammal extends Animal {
    public Mammal(String name){
        super(name);

    }

    @Override
    public String toString() {
        return "Mammal:  Mammal [" + super.toString() + "]";
    }

    
}
