package com.devcamp.task56b70;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Task56B70Application {

	public static void main(String[] args) {
		SpringApplication.run(Task56B70Application.class, args);
	}

}
