package com.devcamp.task56b70.Controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task56b70.Service.AnimalService;
import com.devcamp.task56b70.models.Cat;
import com.devcamp.task56b70.models.Dog;

@RestController
@CrossOrigin
public class AnimalAPI {
    @Autowired
    private AnimalService animalService;
    @GetMapping("/cats")
    public ArrayList<Cat> getCat() {
        ArrayList<Cat> cats = animalService.getAllCat();

    
        return cats;
    }
    @GetMapping("/dogs")
    public ArrayList<Dog> getDog() {
        ArrayList<Dog> dogs = animalService.getAllDog();

        
        return dogs;
    }

}
